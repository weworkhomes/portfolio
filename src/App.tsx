import React from "react";
import ActiveLastBreadcrumb from "./components/Breadcrumb";
import { Routes, Route, BrowserRouter as Router } from "react-router-dom";
import Landing from "./components/Landing/Landing";
import Renovations from "./components/Renovations";
import Background from "./components/Background";
import Art from "./components/Art";

function App() {
  return (
    <div className="App">
      <div style={{ display: "flex", flexDirection: "column" }}>
        <div className="side-image-background" />

        <Background />

        <Router>
          <ActiveLastBreadcrumb />
          <div style={{ margin: "1rem" }}>
            <Routes>
              <Route path="/renovation-work" element={<Renovations />} />
              <Route path="/art" element={<Art />} />
              <Route path="*" element={<Landing />} />
              {/* <Route path="/*" component={LoginComponent} /> */}
            </Routes>
          </div>
        </Router>
      </div>
    </div>
  );
}

export default App;
