import * as React from 'react';
import './Background.css'

export default function Background() {
  return (
    <div className="darken-bg">
     <div className='side-image-background'/>
    </div>
  );
}
