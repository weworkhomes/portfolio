import React from "react";
import Breadcrumbs from "@mui/material/Breadcrumbs";
import Link from "@mui/material/Link";
import { useNavigate, useLocation } from "react-router-dom";

const PATH_NAMES = {
  HOME: "/",
  RENOVATION_WORK: "/renovation-work",
  ART: "/art",
};

export default function ActiveLastBreadcrumb() {
  const navigate = useNavigate();
  const location = useLocation();

  function navigateTo(location: string = PATH_NAMES.HOME) {
    return (event: React.MouseEvent) => {
      event.preventDefault();
      navigate(location);
    };
  }

  function getTextColor(itemPathName: string = PATH_NAMES.HOME) {
    if (itemPathName === PATH_NAMES.HOME && location.pathname === "portfolio")
      return "seashell";
    return location.pathname === itemPathName ? "seashell" : "inherit";
  }

  return (
    <div role="presentation" style={{ padding: "1rem", display: "flex" }}>
      <Breadcrumbs
        aria-label="breadcrumb"
        style={{ marginLeft: "auto", color: "gray", fontSize: "1rem" }}
      >
        <Link
          underline="none"
          color={getTextColor()}
          href={PATH_NAMES.HOME}
          onClick={navigateTo()}
        >
          Home
        </Link>
        <Link
          underline="none"
          href={PATH_NAMES.RENOVATION_WORK}
          color={getTextColor(PATH_NAMES.RENOVATION_WORK)}
          onClick={navigateTo(PATH_NAMES.RENOVATION_WORK)}
        >
          Renovation Work
        </Link>
        <Link
          underline="none"
          href={PATH_NAMES.ART}
          color={getTextColor(PATH_NAMES.ART)}
          onClick={navigateTo(PATH_NAMES.ART)}
        >
          Art
        </Link>
      </Breadcrumbs>
    </div>
  );
}
