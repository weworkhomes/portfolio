export interface ISkill {
    title: string;
    text: string;
}

export const skills: ISkill[] = [
    {
        title: 'Traditional Craftsperson',
        text: `Wall Murals / Paint Effects

        Wooden Doors - Internal
        
        Wooden Shutter`
    },
    {
        title: 'Bricklayer',
        text: `Garden Wall

        Chimney Building / Repair
        
        Pointing / Repointing`
    },
    {
        title: 'Carpenter / Joiner',
        text: `Wooden Staircases

        Wooden Doors - Internal
        
        Wooden Shutter`
    },
    {
        title: 'Electrician',
        text: `Air Conditioning / Refrigeration

        Internal Lighting
        
        Electrical Installation / Testing`
    },
    {
        title: 'Gardener / Landscape gardeners',
        text: `Wooden / Metal / Wire Fences

        Wooden / Metal Gates
        
        Garden Design`
    },
    {
        title: 'Loft Conversion Specialist',
        text: `Home Improvements

        Loft Conversion
        
        Metal Staircases`
    },
    {
        title: 'Plasterer / Renderer',
        text: `Screeding

        Decorative Cornicing / Plasterwork
        
        Internal Rendering`
    },
    {
        title: 'Roofer',
        text: `Chimney Building / Repair

        Guttering and Rainwater Pipe
        
        Thatched Roof`
    },
    {
        title: 'Stoneworker / Stonemason',
        text: `Polished Concrete

        Kitchen Worktops - Stone
        
        Garden Wall`
    },
    {
        title: 'Window fitter / Conservatory installer',
        text: `Conservatory

        Decorative Glazing
        
        Single / Double Glazing`
    },
    {
        title: 'Handyperson',
        text: `Flat Pack Furniture Assembly

        Home Maintenance / Repair
        
        Skirting Board Installation`
    },
    {
        title: 'Bathroom fitter',
        text: `Bathroom Design

        Bathroom Installation
        
        Complete Bathroom Refurbishment`
    },
    {
        title: 'Builder',
        text: `Cellar & Basement Conversion

        Conservatory
        
        Garage Conversion`
    },
    {
        title: 'Painter and decorator',
        text: `Internal Painting & Decorating

        Wall Murals / Paint Effects
        
        External Wall Painting`
    },
    {
        title: 'Floor fitter',
        text: `Electric Underfloor Heating

        Carpet Fitters
        
        Floor Sanding & Finishing`
    },
    {
        title: 'Kitchen Specialist',
        text: `Bespoke Kitchens

        Kitchen Design / Installation
        
        Fitted Kitchens`
    },
    {
        title: 'Blacksmith / Metal worker',
        text: `Metal Staircases

        Metal Kitchen Worktops
        
        Wooden / Metal / Wire Fences`
    },
    {
        title: 'Plumber',
        text: `Bathroom Installation

        Bathroom, Kitchen and WC Plumbing
        
        Plumbing Repair & Maintenance`
    },
    {
        title: 'Specialist Tradesperson',
        text: `Cellar & Basement Conversion

        Air Conditioning / Refrigeration
        
        Fireplace`
    },
    {
        title: 'Tiler',
        text: `Floor Tiling

        Wall Tiling
        
        External Tiling`
    },
    {
        title: 'Cleaner',
        text: `Carpet Cleaning

        Office / Commercial Cleaning
        
        Domestic House Cleaning: One Off`
    }
];
