import {
  Timeline,
  TimelineConnector,
  TimelineContent,
  TimelineDot,
  TimelineItem,
  TimelineSeparator,
} from "@mui/lab";
import { Card, CardContent, Typography } from "@mui/material";
import "./Skills.css";
import { ISkill, skills } from "./skillsData";

function SkillCard({ title, text }: ISkill) {
  return (
    <Card
      sx={{
        backgroundColor: "white",
        opacity: 1,
        zIndex: 999,
        boxShadow: "2px 2px 3px",
      }}
    >
      <CardContent>
        <Typography
          gutterBottom
          variant="h6"
          component="div"
          style={{ color: "teal" }}
        >
          {title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {text}
        </Typography>
      </CardContent>
    </Card>
  );
}

function generateTimelineItems(listOfSkills: ISkill[]) {
  return listOfSkills.map((skill, i) => (
    <TimelineItem key={`${i}-${skill.title}`}>
      <TimelineSeparator>
        <TimelineDot color="success" />
        <TimelineConnector style={{ backgroundColor: "darkslategray" }} />
      </TimelineSeparator>
      <TimelineContent>
        <SkillCard title={skill.title} text={skill.text} />
      </TimelineContent>
    </TimelineItem>
  ));
}

export default function Skills() {
  return (
    <div className="container">
      <h1 style={{ color: "darkslategray" }}>Skill Set</h1>
      <Timeline position="alternate">{generateTimelineItems(skills)}</Timeline>
    </div>
  );
}
