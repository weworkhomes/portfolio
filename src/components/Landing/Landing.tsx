import * as React from 'react';
import IntroCard from './IntroCard';
import Skills from './Skills';

export default function Landing() {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', textAlign: 'center', width: '100%' }}>
     <div style={{fontSize: '6em', padding: '5rem 15%', color: 'seashell', fontFamily: 'Comforter Brush, cursive', fontStyle: 'italic' }}>I'm victorious in Jesus Christ alone</div>
     <IntroCard/>
     <Skills/>
    </div>
  );
}
