import { EmailOutlined } from "@mui/icons-material";
import { Avatar, Card, CardContent, Link } from "@mui/material";
import AvatarPic from "../../assets/images/avatar.png";

export default function IntroCard() {
  return (
    <Card sx={{ maxWidth: "800px", borderRadius: "50px", opacity: 0.75 }}>
      <CardContent>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            backgroundColor: "ivory",
            alignItems: "center",
            flexWrap: "wrap",
          }}
        >
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              padding: "0 2rem",
            }}
          >
            <h1 style={{ marginBottom: "0", color: "darkslategray" }}>
              Joseph Oribhabor
            </h1>
            <h3 style={{ marginTop: "5px", color: "dimgray" }}>
              Traditional Craftsperson
            </h3>
            <Avatar
              sx={{ height: "12em", width: "12em", margin: "2rem" }}
              alt="Joseph"
              src={AvatarPic}
            />
          </div>
          <div
            style={{
              wordWrap: "break-word",
              flexGrow: 1,
              width: "30%",
              padding: "1rem 2rem",
              fontSize: 18,
              textAlign: "justify",
            }}
          >
            <h3
              style={{
                marginTop: 0,
                textAlign: "right",
                color: "darkslategray",
              }}
            >
              About WeWork Homes
            </h3>
            <div>
              Hi There! I'm Joseph Oris, a talented & Passionate craftman, with
              multiple skill set in all home and office improvement works,
              certified and technically inclined... All started from adolescent
              age... Together with my lovely wife KoKo and daughters named my
              business as "WeWork craft" as a result of our passion in Handy
              works.
              <div style={{ textAlign: "initial", marginTop: "1rem" }}>
                <Link
                  underline="hover"
                  variant="h6"
                  gutterBottom
                  href="mailto:weworkhomes@gmail.com"
                  target="_top"
                >
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <EmailOutlined style={{ marginRight: "1rem" }} />
                    <span>weworkhomes@gmail.com</span>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <Link
          underline="hover"
          variant="h6"
          gutterBottom
          href="https://www.ratedpeople.com/profile/we-work"
          target="_blank"
        >
          See my reviews here on ratedpeople.com
        </Link>
      </CardContent>
    </Card>
  );
}
