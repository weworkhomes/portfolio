import Gallery from "./Gallery";

export default function Renovations() {
  function importAll(r: __WebpackModuleApi.RequireContext) {
    let images = {};
    r.keys().forEach((item: string) => {
      images[item.replace("./", "").replaceAll(/\.(png|jpe?g|svg)$/g, "")] =
        r(item);
    });
    return images;
  }

  const images = importAll(
    require.context(
      "../assets/images/renovationWork",
      false,
      /\.(png|jpe?g|svg)$/
    )
  );

  return <Gallery images={images} />;
}
