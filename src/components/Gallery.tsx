import {
  Button,
  ImageList,
  ImageListItem,
  ImageListItemBar,
  Typography,
} from "@mui/material";
import { useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import { Navigation, Scrollbar, A11y } from "swiper";
import { Close } from "@mui/icons-material";

import "./Gallery.css";

export default function Gallery({ images = {} }) {
  const [initialSlide, setInitialSlide] = useState(-1); // -1 means not showing swiper
  const [showTitle, setShowTitle] = useState(-1);

  return (
    <>
      {initialSlide >= 0 ? (
        <div>
          <div style={{ width: "100%", display: "flex" }}>
            <div style={{ flexGrow: 1, color: "white" }}></div>
            <Button
              startIcon={<Close />}
              onClick={() => setInitialSlide(-1)}
              color="info"
            >
              Close
            </Button>
          </div>
          <Swiper
            navigation
            modules={[Navigation, Scrollbar, A11y]}
            initialSlide={initialSlide}
            loop={true}
          >
            {Object.entries(images).map(([title, img]: [string, string], i) => (
              <SwiperSlide>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                  className="slide-container"
                >
                  <img src={img} alt={title} className="slide-img" />
                  <Typography
                    variant="h6"
                    style={{
                      color: "ivory",
                      padding: "0.5rem",
                      marginTop: "1rem",
                      opacity: "0.75",
                    }}
                  >
                    {title}
                  </Typography>
                  <Typography
                    variant="subtitle1"
                    style={{
                      color: "ivory",
                      padding: "0.5rem",
                      opacity: "0.75",
                      marginTop: "-1rem",
                    }}
                  >
                    {`${i + 1} / ${Object.keys(images).length}`}
                  </Typography>
                  <div style={{ flexGrow: 1 }}></div>
                </div>
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
      ) : (
        <ImageList cols={3} gap={10} variant="masonry">
          {Object.entries(images).map(([title, img]: [string, string], i) => (
            <ImageListItem
              key={`${i}-${title}`}
              onClick={() => setInitialSlide(i)}
              onMouseOver={() => setShowTitle(i)}
              onMouseOut={() => setShowTitle(-1)}
              className="img-preview"
            >
              <img src={img} loading="lazy" alt={title} />
              {showTitle === i && (
                <ImageListItemBar
                  style={{ textAlign: "center" }}
                  title={title}
                />
              )}
            </ImageListItem>
          ))}
        </ImageList>
      )}
    </>
  );
}
